try
{
	var IncomingIP = context.getVariable('proxy.client.ip');
	var whitelistedIPs = context.getVariable('whitelistedIPs');
	var errorJSON;
	var exceptionName='';
	
	if(whitelistedIPs)
	{
		whitelistedIPs = whitelistedIPs.replace(/\s/g,'');
		whitelistedIPsArray = whitelistedIPs.split(",");
		if(whitelistedIPsArray.indexOf(IncomingIP) == -1)
		{
			errorJSON = 'invalid_ip';
			context.setVariable('errorJSON1',errorJSON);
			exceptionName = 'invalid_ip';
			throw exceptionName;
		}
	}
	else
	{
		errorJSON = 'internal_config_error';
		context.setVariable('errorJSON1',errorJSON);
		exceptionName = 'internal_config_error';
		throw exceptionName;
	}
}
catch(err){
    if(!errorJSON)
    {
        context.setVariable('errorJSON','internal_config_error');
        throw err;
    }
    else
    {
        throw exceptionName;
    }
}
